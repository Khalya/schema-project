from django.urls import path

from beast.views import IndexTemplateView

urlpatterns = [
    path('', IndexTemplateView.as_view(), name='index')
]
