from django.db import models


# Create your models here.
class StandartObject(models.Model):
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


class BeastView(StandartObject):
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Вид животного'
        verbose_name_plural = 'Виды животного'


class Breed(StandartObject):
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Порода животного'
        verbose_name_plural = 'Порода животного'


class City(StandartObject):
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Beast(models.Model):
    class GenderChoices(models.IntegerChoices):
        male = 0, 'Самец',
        female = 1, 'Самка'

    beast_view = models.ForeignKey(
        to=BeastView,
        verbose_name='Вид животного',
        on_delete=models.DO_NOTHING,
        related_name='beasts'
    )
    breed = models.ForeignKey(
        to=Breed,
        verbose_name='Порода животного',
        on_delete=models.DO_NOTHING,
        related_name='beasts'
    )
    gender = models.SmallIntegerField(
        verbose_name='Пол',
        default=0,
        choices=GenderChoices.choices
    )
    birthday_date = models.DateField(
        verbose_name='Дата рождения',
        null=True,
        blank=True
    )
    location = models.CharField(
        verbose_name='Локация (место где животное должно быть)',
        max_length=255
    )
    city = models.ForeignKey(
        to=City,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='Город'
    )
    created = models.DateField(
        verbose_name='Создан',
        auto_now_add=True
    )

    def __str__(self):
        return f"{self.beast_view, self.breed, self.get_gender_display(), self.city}"

    class Meta:
        verbose_name = 'Животное'
        verbose_name_plural = 'Животные'
