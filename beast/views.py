import datetime

from django.db.models import Count
from django.views.generic import TemplateView
import qsstats

from beast.models import Beast, City, BeastView, Breed


class IndexTemplateView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        start_date = datetime.date(2021, 1, 1)
        end_date = datetime.date(2021, 12, 31)
        city = self.request.GET.get('city', None)
        view = self.request.GET.get('view', None)
        breed = self.request.GET.get('breed', None)
        query = Beast.objects.all()
        query_kwargs = {}
        context['city_id'] = city
        if city:
            query_kwargs['city_id'] = city
            context['city_title'] = City.objects.get(id=city)
        if view:
            query_kwargs['beast_view_id'] = view
        if breed:
            query_kwargs['breed_id'] = breed
        beasts = query.filter(**query_kwargs)
        if not query_kwargs:
            beasts = Beast.objects.all()
            context['all'] = True
        qs = qsstats.QuerySetStats(beasts, date_field='created')
        time_series = qs.time_series(start_date, end_date, interval='months')
        context['graphic'] = [x[1] for x in time_series]
        context['beasts'] = Beast.objects.filter(created__range=[start_date, end_date])
        context['cities'] = City.objects.all()
        context['beast_views'] = BeastView.objects.all()
        context['breeds'] = Breed.objects.all()
        return context
