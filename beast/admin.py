from .models import Beast, BeastView, Breed, City
from django.contrib import admin


@admin.register(Beast)
class BeastAdmin(admin.ModelAdmin):
    pass

@admin.register(BeastView)
class BeastViewAdmin(admin.ModelAdmin):
    pass

@admin.register(Breed)
class BreedAdmin(admin.ModelAdmin):
    pass

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    pass
